<%

'***************************************************************************
'
'  Nome da Fun�ao	: DoLog
'  Parametros		: vaArquivo, vaMSG
'  Prop�sito		: Grava log em determinado vaArquivo
'  Retorno			: --
'
'***************************************************************************
Public Sub DoLog(vaArquivo,vaMSG)
	
	Dim ObjFSO, ObjFile, ObjStream, Dia, Mes, Ano, Hora, IpNum
	
	Const forReading = 1, forWriting = 2, forAppending = 8
	Const TriDef = -2, TriTrue = -1, TriFalse = 0
	
	Set ObjFSO = CreateObject("Scripting.FileSystemObject")
 	
   	If ObjFSO.FileExists(vaArquivo) = False then
	   objFSO.CreateTextFile(vaArquivo)
	End If

	Set ObjFile = objFSO.GetFile(vaArquivo)
	Set objStream = ObjFile.OpenAsTextStream(forAppending,TriDef)
	
	Dia = Day(Date())
	Mes = Month(Date())
	Ano = Year(Date())
	Hora = Time()
	IpNum = Request.ServerVariables("REMOTE_ADDR")

	ObjStream.WriteLine  "<div>" & Dia & "-" & Mes & "-" & Ano & " | " & Hora & " | " & IpNum & " | " & vaMSG & "</div>"
	ObjStream.close

End Sub


'***************************************************************************
'
'  Nome da Fun�ao	: SetErro
'  Parametros		: vaErro
'  Prop�sito		: Trata erro na rotina
'  Retorno			: String de retorno do servi�o
'
'***************************************************************************
Function SetErro(vaErro)

	' Gerar o erro no escopo do servi�o
	vgCodError = vaErro

    if Session("log_erro") = "ON" then DoLog Session("sub_log_erro"), vaErro
	Response.Redirect("wap_erros.wml?Coderro=" & vaErro)

End Function

%>