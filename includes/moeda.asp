<%
'***************************************************************************
'
'  Nome da Fun�ao	: ConverteMoeda
'  Parametros		: vaValor
'  Prop�sito		: Converte uma string com um valor num�rico em Moeda
'  Retorno			: Valor formatado em moeda
'
'***************************************************************************
Function ConverteMoeda(vaValor)

	Dim vlIndex
	Dim vlValorConvertido
	Dim vlCentavos
	Dim vlMilhar
	Dim vlMilhao
	Dim vlBilhao
	Dim vlTrilhao

	' Remover espa�os extras
	vaValor=trim(vaValor)

	' Verificar se algum valor foi passado como par�metro
	if (vaValor="") then exit function

	' Verificar se o valor do parametro passado era igual a zero
	if cDbl(vaValor)=cDbl(0) then

	    ' Atribuir o valor de zero
	    vlValorConvertido="0,00"

	else

	    vlValorConvertido=""
	    i=len(vaValor)	' Tamanho � 15

	    Do while i>0

		If cDbl(Mid(vaValor,1,i))<>0 or i>12 then
		    Temp=Mid(vaValor,i,1)

		    Select Case i

			case 13
			    vlValorConvertido= temp & "," & vlValorConvertido
			case 10,7,4,1
			    vlValorConvertido= temp & "." & vlValorConvertido
			case else
			    vlValorConvertido= temp & vlValorConvertido
		    
		    end select
		end if
		i=i-1
	    loop
	End if

	' Retornar o valor da fun�ao
	ConverteMoeda = vlValorConvertido

End Function


'***************************************************************************
'
'  Nome da Fun�ao	: ConverteSinal
'  Parametros		: vaPosicao
'  Prop�sito		: Converte o sinal do Mainframe em "-" ou "+"
'  Retorno			: Sinal
'
'***************************************************************************
Function ConverteSinal(vaPosicao)

	Dim vlSinal

	' Atribuir o valor do sinal positivo padr�o
	vlSinal=""
		
	' Recuperar o Sinal do Mainframe e atribuir se necess�rio
	if ucase(Mid(vsString,vaPosicao,1))="D" then vlSinal="-"

	' Retornar o Sinal
	ConverteSinal = vlSinal

End Function
%>