<!--#INCLUDE FILE="HeadString.asp" -->

<%

'***************************************************************************
'
'  Nome da Fun�ao	: ExecutaServico
'  Parametros		: vaServico
'  Prop�sito		: Executa o servi�o no CICS NT
'  Retorno			: String de retorno do servi�o
'
'***************************************************************************
Function ExecutaServico(vaServico)


' Declara��o de Vari�veis
dim objCicsEPI				' Objeto para comunica��o com o Mainframe
Dim vlResutaldoCICS			' Resultado do envio de comunica��o atrav�s da EPI (Controle de Erro)
dim vsUsuario				' Usu�rio para a chamada do Terminal
dim vsPassword				' Senha para a chamada do Terminal
dim vsServidor				' Servidor CICS
dim enumEnter				' Enumera��o da tecla ENTER do Mainframe
Dim vlStringRetorno			' String retornado da execu��o do servi�o
Dim vlStatusErro			' Status do Erro retornado pelo Mainframe da Execu��o do Servi�o


' Inicializa��o de Enumera��es
enumEnter = 0

' Atribuir as informa��es do processo de Login
vsServidor = "CICSTCP"
vsUsuario = "MWAPNED" '"MPLATNED"
vsPassword = "EEPDUPDN"
vlResutaldoCICS = true


' Instanciar o objeto de comunica��o com o CICS
Set objCicsEPI = CreateObject("CicsEPI.BBVEPI")

' Verificar se ocorreu erro na Inst�ncia do Objeto
if not (isObject(objCicsEPI)) then

	' N�o foi poss�vel instanciar o objeto de comunica��o do CICS
	' Gerar erro informando
	SetErro 001

End if

' Se n�o ocorreu erro na opera��o anterior
if (vgCodError = 0) then

	' Iniciar o processo de comunica��o com o CICS
	vlResutaldoCICS = objCicsEPI.InitEPI(CStr(vsServidor), CStr(vsUsuario), CStr(vsPassword))

	' Analisar o resultado da inicia��o da conex�o CICS
	if 	(vlResutaldoCICS = false) then
		SetErro 002
	end if

End if

' Se n�o ocorreu erro na opera��o anterior
if (vgCodError = 0) then

    if Session("trace") = "ON" then DoLog Session("sub_trace"),vaServico

	' Enviar o string do servi�o
	vlResutaldoCICS = objCicsEPI.SendEPI(CStr(vaServico), CInt(enumEnter))

	' Analisar o resultado da execu��o do servi�o no CICS
	if 	(vlResutaldoCICS = false) then
		SetErro 003
	end if

End if

' Se n�o ocorreu erro na opera��o anterior
if (vgCodError = 0) then

	' Retornar o resultado do servi�o
	vlStringRetorno = CStr(objCicsEPI.Buffer)

    if Session("trace") = "ON" then DoLog Session("sub_trace"),vlStringRetorno

	' Finalizar o processo de comunica��o com o CICS
	vlResutaldoCICS = objCicsEPI.EndEPI("CICSTCP")

	' Analisar o resultado da finaliza��o da conex�o CICS
	if 	(vlResutaldoCICS = false) then
		SetErro 004
	end if

End if

' Se n�o ocorreu erro na opera��o anterior
if (vgCodError = 0) then

    ' Recuperar o status de erro retornado pelo Mainframe
    vlStatusErro = Mid( vlStringRetorno, 137, 2 )
 
    ' Se ocorreu erro na solicita�ao do servi�o do Mainframe
    if (vlStatusErro <> "00") AND (vlStatusErro <> "99") then

		' Erro na recupera�ao dos dados
        ' Concatenar as informa�oes de Erro do MainFrame
		vlCodErro = "10" & cStr(Mid( vlStringRetorno, 134, 5 ))

		' Gerar erro informando
		SetErro vlCodErro

    end if

End if

' Se n�o ocorreu erro na opera��o anterior
if (vgCodError = 0) then

	' Retornar o string da execu��o do servi�o
	ExecutaServico = vlStringRetorno

End if

' Eliminar o objeto CICS da mem�ria
Set objCicsEPI = Nothing
		
End Function

%>