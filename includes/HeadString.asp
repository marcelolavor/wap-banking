<%

'***************************************************************************
'
'  Nome da Fun�ao	: HeadString
'  Parametros		: vaServico - Servi�o a ser montado
'  Prop�sito		: Monta o cabe�alho de comunica��o com o CICS
'  Retorno			: Cabe�alho de comunica��o com CICS
'
'***************************************************************************
Function HeadString(vaServico)

	' Declara��o de vari�veis locais
	Dim vlString, vlAgencia, vlConta, vlCodAcesso, vlEtapaAtual, vlEtapaTotal, vlCodControle, vlIPCliente

	' Atribui��o das vari�veis de conex�o
	vlServico		= vaServico
	vlAgencia		= Session("Agencia")
	vlConta			= Session("Conta")
	vlCodAcesso		= Session("CodAcesso")
	vlEtapaAtual	= Session("EtapaAtual")
	vlEtapaTotal	= Session("TotalEtapas")
	vlCodControle	= Session("CodControle")
	vlIPCliente		= Session("IPCliente")

	' Montar o string
	vlString = StringFixo(vlServico,vlAgencia,vlConta,vlCodAcesso,vlEtapaAtual,vlEtapaTotal,vlCodControle,vlIPCliente)

	' Retornar o String
	HeadString = vlString
	
End function


'***************************************************************************
'
'  Nome da Fun�ao	: HeadString
'  Parametros		: vaServico - Servi�o a ser montado
'  Prop�sito		: Monta o cabe�alho de comunica��o com o CICS
'  Retorno			: Cabe�alho de comunica��o com CICS
'
'***************************************************************************
Function StringFixo(vaServico,vaAgencia,vaConta,vaCodigo,vaEtapaAtual,vaEtapaTotal,vaCodControle,vaIPCliente)

	' Montar a requisi��o do servi�o de Saldo de Conta Corrente
	vlString =	"00WPS01" & FormataNumero(vaServico,3) & "E" & _
				FormataNumero(vaAgencia,4) & FormataNumero(vaConta,13) & _
				FormataNumero(vaCodigo,5) & FormataNumero("",4) & _
				FormataNumero("",9) & "A" & FormataNumero(vaIPCliente,15) & _
				"1" & FormataNumero(vaCodControle,9) & FormataNumero("",15) & _
				FormataNumero(vaEtapaAtual,1) & FormataNumero(vaEtapaTotal,1) & _
				FormataNumero("",311)

	StringFixo = vlString
	
End function

'***************************************************************************
'
'  Nome da Fun�ao	: FormataNumero
'  Parametros		: vaNumero - String a ser formatado, vaTamanho - Tamanho final do string
'  Prop�sito		: Formata um String para que seja enviado para o CICS
'  Retorno			: --
'
'***************************************************************************
Private Function FormataNumero(vaNumero, vaTamanho)

    Dim vlIndex, vlZero
    vlZero = ""

    for vlIndex = 1 to (vaTamanho - len(vaNumero))
		vlZero = vlZero & "0"
    next

    ' Retornar o valor da Fun�ao
    FormataNumero = vlZero & vaNumero

End Function

%>