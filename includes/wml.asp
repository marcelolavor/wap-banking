<%

' *** ESCREVE NO ARQUIVO XML ***
Sub wapWrite(vaTexto)

	response.write(vaTexto)
	
End Sub


' *** ESCREVE TEXTO XML ***
Sub wapWriteTexto(vaTexto, vaAlinhamento)

	Dim vlAlinhamento(4)

	vlAlinhamento(0) = ""
	vlAlinhamento(1) = " align=left"
	vlAlinhamento(2) = " align=center"
	vlAlinhamento(3) = " align='right'"			

	response.write("<p" & vlAlinhamento(vaAlinhamento) & ">" & vaTexto & "</p>")
	
End Sub

%>