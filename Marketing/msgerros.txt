0000213 Prezado(a) cliente, nao foi possivel completar sua operacao. Ocorreu um erro na comunicacao. Verifique no seu historico se sua transacao foi realizada. 
0000500	O servico nao esta disponivel no momento. Por favor tente mais tarde. 
0000501	A agencia nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000502	A agencia nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000503	A agencia nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000504	A Conta Corrente nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000505	A Conta Corrente nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000506	A Conta Corrente nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000507	As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
0000508	As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
0000509	As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
0000510	A nova Senha nao esta de acordo com sua confirmacao. Retorne a pagina anterior e tente novamente. 
0000511	A Senha Internet nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000512	A Senha Internet nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
0000513	O codigo do banco nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000514	O codigo do banco nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000515	O codigo do banco nao foi informado. Retorne a pagina anterior e tente novamente. 
0000516	O nome nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000517	O nome nao foi informado. Retorne a pagina anterior e tente novamente. 
0000518	O valor nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000519	O valor nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000520	O valor nao foi informado. Retorne a pagina anterior e tente novamente. 
0000521	O CPF nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000522	O CPF nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000523	O CPF ou CGC nao foi informado. Retorne a pagina anterior e tente novamente. 
0000524	O CGC nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000525	O CGC nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000526	Somente o CPF ou o CGC devera ser informado. Retorne a pagina anterior e tente novamente. 
0000527	A data informada esta incorreta. Retorne a pagina anterior e tente novamente. 
0000528	A data nao foi informada. Retorne a pagina anterior e tente novamente. 
0000529	O numero do codigo de barras nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000530	O numero do codigo de barras nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000531	O numero do codigo de barras nao foi informado. Retorne a pagina anterior e tente novamente. 
0000532	O periodo maximo para consulta de extrato e de 30 dias. Retorne a pagina anterior e informe novo periodo. 
0000533	O CPF nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000534	O CGC nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000535	A senha nao foi informada corretamente. Retorne a pagina anterior e tente novamente
0000536	A senha nao foi informada corretamente. Retorne a pagina anterior e tente novamente
0000537	A senha nao foi informada corretamente. Retorne a pagina anterior e tente novamente
0000538	As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
0000539	As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
0000540	O codigo numerico do Bloqueto de pagamento nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000541	O codigo numerico do Bloqueto de pagamento nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000542	A leitora otica nao gerou corretamente o codigo de barras. Retorne a pagina anterior e tente novamente. 
0000543	A leitora otica gerou caracteres invalidos para o codigo de barras. Retorne a pagina anterior e tente novamente. 
0000544	Digite o codigo do Bloqueto ou forneca o Codigo pela leitora otica. Retorne a pagina anterior e tente novamente. 
0000545	O codigo do Bloqueto de pagamento nao foi informado. Retorne a pagina anterior e tente novamente. 
0000546	O servico de pagamento selecionado nao esta adequado. Retorne a pagina anterior e selecione a opcao de pagamento de Cobranca Boavista 
0000547	O servico de pagamento selecionado nao esta adequado. Retorne a pagina anterior e selecione a opcao de pagamento de Cobranca a outros Bancos. 
0000548	O periodo maximo para consulta de extrato e de 30 dias. Retorne a pagina anterior e informe nova data inicial. 
0000549	O tipo de operacao a ser realizada no Fundo de Investimento nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000550	O Codigo do Investidor nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000551	O numero do Cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000552	O numero do Cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000553	O numero do Cheque nao foi informado. Retorne a pagina anterior e tente novamente. 
0000554	O numero do Renavam nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000555	O numero do Renavam nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
0000556	O numero do Renavam nao foi informado. Retorne a pagina anterior e tente novamente. 
0000557 O endereco nao foi preenchido. Retorne a pagina anterior. 
0000558 O endereco nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000559 O endereco nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000560 O endereco nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000561 O endereco nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000562 O tamanho do complemento nao esta correto. Retorne a pagina anterior e tente novamente. 
0000563 O tamanho do bairro nao esta correto. Retorne a pagina anterior e tente novamente. 
0000564 O bairro nao foi preenchido. Retorne a pagina anterior e tente novamente. 
0000565 O tamanho da cidade nao esta correto. Retorne a pagina anterior e tente novamente. 
0000566 A cidade nao foi preenchida. Retorne a pagina anterior e tente novamente. 
0000567 O tamanho do CEP nao esta correto. Retorne a pagina anterior e tente novamente. 
0000568 O CEP nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000569 O CEP nao foi preenchido. Retorne a pagina anterior e tente novamente. 
0000570 O tamanho da UF esta nao esta correto. Retorne a pagina anterior e tente novamente. 
0000571 A UF nao foi preenchida corretamente. Retorne a pagina anterior e tente novamente. 
0000572 A UF nao foi preenchida. Retorne a pagina anterior e tente novamente. 
0000573 O tamanho do Email nao esta correto. Retorne a pagina anterior e tente novamente. 
0000574 O Email nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000575 O Email nao foi preenchido. Retorne a pagina anterior e tente novamente. 
0000576 O tamanho do telefone nao esta correto. Retorne a pagina anterior e tente novamente. 
0000577 O telefone nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000578 O telefone nao foi preenchido. Retorne a pagina anterior e tente novamente. 
0000579 O tipo nao foi preenchido. Retorne a pagina anterior e tente novamente. 
0000580 O tamanho do fax nao esta correto. Retorne a pagina anterior e tente novamente. 
0000581 O fax nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000582 O numero do cartao nao foi informado. Retorne a pagina anterior e tente novamente. 
0000583 A forma de pagamento nao foi informada. Retorne a pagina anterior e tente novamente. 
0000584 O tamanho do DDD nao esta correto. Retorne a pagina anterior e tente novamente. 
0000585 O DDD nao foi preenchido corretamente. Retorne a pagina anterior e tente novamente. 
0000586 O DDD nao foi preenchido. Retorne a pagina anterior e tente novamente. 
0000587 O numero informado nao pertence a uma operadora habilitada. Retorne a pagina anterior e tente novamente. 
0000588 A operadora nao foi informada. Retorne a pagina anterior e tente novamente. 
0000589 A operadora nao foi preenchida corretamente. Retorne a pagina anterior e tente novamente. 
0000590 O tamanho da operadora nao esta correto. Retorne a pagina anterior e tente novamente. 
0000591 A operadora nao e valida. Retorne a pagina anterior e tente novamente. 
1000103 A agencia nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1000104 O numero da Conta Corrente nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000105 O numero do codigo de acesso nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000109 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000110 A senha do cartao nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1000202 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000203 Seu codigo de acesso esta bloqueado. 
1000205 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000206 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000207 Seu codigo de acesso esta bloqueado. 
1000208 Seu codigo de acesso esta bloqueado. 
1000211 Seu codigo de acesso encontra-se em processo de reemissao. 
1000213 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000219 A Senha Internet nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1000220 A Senha Internet nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1000223 A Senha Internet nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1000233 O numero do cartao nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000313 O valor informado esta abaixo do valor minimo permitido para esta operacao. Retorne a pagina anterior e tente novamente. 
1000314 O valor informado esta acima do valor maximo permitido para esta operacao. Retorne a pagina anterior e tente novamente. 
1000323 A operacao solicitada nao esta disponivel para pessoa fisica. 
1000324 A operacao solicitada nao esta disponivel para pessoa juridica. 
1000325 A operacao solicitada nao esta disponivel para pessoa fisica/juridica. 
1000326 A data informada nao e valida para este servico. Retorna a pagina anterior e tente novamente. 
1000327 A data informada nao e valida para este servico. Retorna a pagina anterior e tente novamente. 
1000328 A data informada nao e um dia util. Retorna a pagina anterior e tente novamente. 
1000330 O valor informado esta acima do valor maximo permitido para esta operacao. Retorne a pagina anterior e tente novamente. 
1000331 Esta operacao nao permite agendamento. 
1000333 O valor informado esta acima do valor maximo permitido para esta operacao. Retorne a pagina anterior e tente novamente. 
1000334 O valor informado esta abaixo do valor minimo permitido para esta operacao. Retorne a pagina anterior e tente novamente. 
1000335 O servico solicitado esta fora do horario permitido. 
1000336 O agendamento de operacoes nao esta disponivel para sua Conta Corrente. 
1000337 A data informada e inferior a data atual. Retorne a pagina anterior e preencha a data novamente. 
1000344 Sua Conta Corrente esta bloqueada. 
1000345 Sua Conta Corrente esta bloqueada. 
1000346 O codigo de barras nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000347 Infelizmente a concessionaria informada nao esta autorizada pelo Boavista. Para sua informacao utilize a Consulta de Concessionarias. 
1000355 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000356 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000357 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000358 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1000359 A data informada nao e valida. Retorne a pagina anterior e preencha a data novamente. 
1000518 O valor solicitado para a operacao e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1000702 A data informada esta incorreta. Retorne a pagina anterior e preencha a data novamente. 
1000703 Nao ha mais lancamentos para esta conta. 
1000704 Nao ha mais lancamentos para esta conta. 
1000706 A data informada esta incorreta. Retorne a pagina anterior e preencha a data novamente. 
1000715 A data informada esta incorreta. Retorne a pagina anterior e preencha a data novamente. 
1000903 O valor solicitado nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000904 O codigo do banco destinatario nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000905 O codigo da agencia do banco destinatario nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000906 Para operacoes de transferencias para o mesmo banco de origem, utilize a opcao de transferencia entre contas. 
1000907 O codigo da Conta Corrente do banco destinatario nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000908 O nome do correntista destinatario nao foi informado. Retorne a pagina anterior e tente novamente. 
1000909 O CPF/CGC do destinatario nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000910 A finalidade do DOC nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1000911 O numero da agencia ou Conta Corrente nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1000913 O valor solicitado para a transferencia e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1000920 O banco e/ou agencia do destinatario nao foram informados corretamente. Retorne a pagina anterior e tente novamente. 
1000921 O banco destinatario nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1001002 A agencia e/ou Conta Corrente nao foram informadas corretamente. Retorne a pagina anterior e tente novamente. 
1001006 Sua Conta Corrente esta bloqueada. 
1001007 Sua Conta Corrente esta bloqueada. 
1001008 O valor solicitado para a transferencia e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1001009 A Conta Corrente de destino esta bloqueada. 
1001010 A Conta Corrente de destino esta bloqueada. 
1001012 A Conta Corrente de destino nao pode ser a mesma do que a conta de origem. Retorne a pagina anterior e tente novamente. 
1001102 A agencia e/ou Conta Corrente nao foram informadas corretamente. Retorne a pagina anterior e tente novamente. 
1001106 Sua Conta Corrente esta bloqueada. 
1001107 Sua Conta Corrente esta bloqueada. 
1001108 O valor solicitado para a transferencia e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1001109 A Conta Corrente de destino esta bloqueada. 
1001110 A Conta Corrente de destino esta bloqueada. 
1001115 A sua Conta Corrente nao possui uma conta de poupanca vinculada. 
1001202 A agencia e/ou Conta Corrente nao foram informadas corretamente. Retorne a pagina anterior e tente novamente. 
1001206 Sua Conta Corrente esta bloqueada. 
1001207 Sua Conta Corrente esta bloqueada. 
1001208 O valor solicitado para a transferencia e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1001209 A Conta Corrente de destino esta bloqueada. 
1001210 A Conta Corrente de destino esta bloqueada. 
1001215 A sua Conta Corrente nao possui uma conta de poupanca vinculada. 
1001302 A agencia e/ou Conta Corrente nao foram informadas corretamente. Retorne a pagina anterior e tente novamente. 
1001305 O codigo de barras nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1001306 Infelizmente a concessionaria informada nao esta autorizada pelo Boavista. Para sua informacao utilize a Consulta de Concessionarias. 
1001307 O seu saldo disponivel e menor que o valor do pagamento. 
1001314 Esta Conta ja foi paga hoje. Verifique o seu Historico de Transacoes Realizadas. 
1001602 A sua Senha Internet nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1001607 A sua Senha Internet nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1001608 A nova Senha Internet informada e igual a Senha Internet atual. Retorne a pagina anterior e tente novamente. 
1001612 A Senha do Cartao nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1001616 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1001622 Seu codigo de acesso esta bloqueado. 
1001702 A sua Conta Corrente nao possui uma conta de poupanca vinculada. 
1001804 A data nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1001805 A data nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1001808 Nao ha historico para consulta. 
1001908 Nao ha historico para consulta. 
1002502 O Numero da agencia ou Conta Corrente nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1002511 O Fundo informado nao foi encontrado. Retorne a pagina anterior e tente novamente. 
1002512 O valor solicitado para a operacao e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1002513 A sua Conta Corrente nao possui nenhum Fundo de Investimento vinculado. 
1002514 Este Fundo de Investimento nao permite este tipo de operacao. 
1002602 O Numero da agencia ou Conta Corrente nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1002611 O Fundo informado nao foi encontrado. Retorne a pagina anterior e tente novamente. 
1002612 O valor solicitado para a operacao e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1002613 A sua Conta Corrente nao possui nenhum Fundo de Investimento vinculado. 
1002614 Este Fundo de Investimento nao permite este tipo de operacao. 
1002708 O numero do cartao nao foi informado. Retorne a pagina anterior. 
1002709 O numero do cartao nao foi informado corretamente. Retorne a pagina anterior. 
1002711 O cartao nao e valido. 
1002712 O valor solicitado para pagamento da fatura, e superior ao saldo disponivel em conta. Retorne a pagina anterior e tente novamente. 
1002724 Esta Conta Corrente nao possui cartao. 
1002725 O valor solicitado e inferior ao valor minimo permitido. Retorne a pagina anterior. 
1002726 O valor solicitado e superior ao valor da fatura. Retorne a pagina anterior. 
1004002 A data nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1004003 Nao ha mais lancamentos para esta conta. 
1004004 Nao ha mais lancamentos para esta conta. 
1004006 A data nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1004009 A agencia nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1004015 A data nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1004025 A sua Conta Corrente nao possui Poupanca Automatica. 
1004102 Os dados da Conta Corrente nao sao validos. 
1004106 Sua Conta Corrente esta bloqueada. 
1004107 Sua Conta Corrente esta bloqueada. 
1004108 O valor solicitado para a transferencia e maior do que seu saldo disponivel. Retorne a pagina anterior e tente novamente. 
1004109 A sua Conta Corrente nao possui Poupanca Automatica. 
1004519 Esta operacao nao pertence a Conta Corrente informada. Retorne a pagina anterior e tente novamente. 
1004605 Sua Conta Corrente nao esta cadastrada para este servico. 
1004702 Sua Conta Corrente nao esta cadastrada para este servico. 
1004706 A operacao solicitada esta invalida. Retorne a pagina anterior e tente novamente. 
1004707 O DDD informado e invalido. Retorne a pagina anterior e tente novamente. 
1004708 O telefone informado e invalido. Retorne a pagina anterior e tente novamente. 
1004709 Este telefone ja esta cadastrado. Retorne a pagina anterior e tente novamente. 
1004804 Nao foi realizada nenhuma alteracao. Retorne a pagina anterior e tente novamente. 
1004814 O DDD nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004815 O DDD nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004816 O telefone nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004817 O telefone nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004817 Esse telefone nao consta no nosso cadastro para alteraao. 
1004912 O DDD nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004913 O DDD nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004914 O telefone nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004915 O telefone nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1004914 A operadora nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1004915 A operadora nao foi informada corretamente. Retorne a pagina anterior e tente novamente. 
1005802 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1005804 A data de pagamento nao foi informada corretamente. Retome a pagina anterior e tente novamente. 
1005806 A data informada no vencimento e inferior a data de hoje. Retorne a pagina anterior e tente novamente. 
1005807 O valor informado para pagamento nao pode ser igual a zero. Retome a pagina anterior e tente novamente. 
1005810 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1005811 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1005812 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1005813 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1005817 O valor solicitado para o pagamento e maior do que seu saldo disponivel. 
1005818 O pagamento solicitado nao consta no nosso cadastro. 
1005821 A data do pagamento nao e valida. Retorne a pagina anterior e tente novamente. 
1005822 O numero informado e inexistente. Retorne a pagina anterior e tente novamente. 
1005825 Este Bloqueto de Cobranca ja esta vencido, nao pode ser pago via Internet. 
1005826 Este Bloqueto de Cobranca esta em Cartorio, nao pode ser pago via Internet. 
1005827 A data de calculo e inferior a data atual. Este boleto de cobranca nao pode ser pago via internet. 
1005828 Sua Conta Corrente esta bloqueada. 
1005829 A Conta Corrente nao e valida para este servico. 
1005830 A Conta Corrente nao e valida para este servico. 
1005831 Sua Conta Corrente esta bloqueada para pagamento. 
1005838 Este Bloqueto de Cobranca ja foi pago hoje. Verifique o seu Historico de Transacoes Realizadas. 
1005843 A data informada no vencimento e inferior a data de hoje. Retorne a pagina anterior e tente novamente. 
1005844 Este Bloqueto de Cobranca nao consta cadastrado na Cobranca Boavista. 
1005845 Este Bloqueto de Cobranca nao pode ser pago via Internet, somente nas agencias Boavista. 
1007230 O numero do renavam nao foi preenchido corretamente. Retorne a pagina anterior. 
1007238 Este municipio nao esta cadastrado para recebimento de IPVA. 
1007309 Este renavam so pode ser pago atraves de cota unica. 
1007310 O pagamento da cota 1 so pode ser efetuado ate a data de vencimento. 
1007311 Este pagamento nao pode ser efetuado. 
1007330 O numero do renavam nao foi encontrado. Retorne a pagina anterior e tente novamente. 
1007338 Este municipio nao esta cadastrado para recebimento de IPVA. 
1008204 Nao houve nehuma alteracao . 
1008213 O endereco nao foi preenchido. Retorne a pagina anterior. 
1008214 O logradouro nao foi preenchido. Retorne a pagina anterior. 
1008215 O numero do logradouro nao foi preenchido. Retorne a pagina anterior. 
1008216 O bairro nao foi preenchido. Retorne a pagina anterior. 
1008217 A localidade nao foi preenchida. Retorne a pagina anterior. 
1008218 A localidade e imconpativel com o CEP. Retorne a pagina anterior. 
1008219 O CEP esta incorreto. Retorne a pagina anterior. 
1008220 O CEP esta incorreto. Retorne a pagina anterior. 
1008221 A UF nao foi preenchida. Retorne a pagina anterior. 
1008222 A UF e incompativel com o CEP. Retorne a pagina anterior. 
1008226 O telefone nao foi informado. Retorne a pagina anterior. 
1008227 O telefone nao foi informado corretamente. Retorne a pagina anterior. 
1008228 O DDD nao foi preenchido corretamente. Retorne a pagina anterior. 
1008229 O telefone nao foi informado corretamente. Retorne a pagina anterior. 
1008230 Ja existe este telefone no cadastro. 
1008231 O DDD do Fax nao foi preenchido corretamente. Retorne a pagina anterior. 
1008232 O numero do FAX nao foi informado corretamente. Retorne a pagina anterior. 
1008304 A agencia e/ou conta nao foram informadas corretamente. Retorne a pagina anterior. 
1008311 A data nao foi informada corretamente. Retorne a pagina anterior. 
1008312 Nao existe mais lancamentos para esta Conta Corrente. 
1008313 Nao existe lancamentos para esta Conta Corrente. 
1008315 A data nao foi informada corretamente. Retorne a pagina anterior. 
1008318 A agencia nao foi informada corretamente . Retorne a pagina anterior. 
1008322 A data nao foi informada corretamente. Retorne a pagina anterior. 
1008323 A data final nao foi informada corretamente. Retorne a pagina anterior. 
1008332 A data nao foi informada corretamente. Retorne a pagina anterior. 
1008333 Nao existe mais lancamentos para esta Conta Corrente. 
1008334 Nao existe mais lancamentos para esta Conta Corrente. 
1008336 A data do periodo nao foi informada corretamente. Retorne a pagina anterior. 
1008351 Esta conta nao possui Poupanca Automatica. 
1009112 O numero do cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1009113 O numero do cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1009114 O numero do cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1009115 As informacoes fornecidas nao sao validas ou os campos nao foramcorretamente preenchidos. Retorne a pagina anterior e tente novamente. 
1009116 O numero do cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1009117 Sua Conta Corrente nao esta autorizada para este servico. 
1009118 Sua Conta Corrente nao esta autorizada para este servico. 
1009119 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo seu Gerente. Faca o recadastramento na sua agencia Boavista. 
1009120 Sua Conta Corrente nao esta autorizada para este servico. 
1009121 Sua Conta Corrente nao esta autorizada para este servico. 
1009125 O Numero do cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1009132 Ja existe uma solicitacao de talao a domicilio pendente. 
1009134 Sua Conta Corrente nao esta autorizada para este servico. 
1009135 Sua Conta Corrente nao esta autorizada para este servico. 
1009136 Sua Conta Corrente nao esta autorizada para este servico. 
1009137 Quantidade de Taloes invalida. Retorne a pagina anterior e tente novamente. 
1009138 Sua Conta Corrente nao esta autorizada para este servico. Faca o recadastramento na sua agencia Boavista. 
1009139 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009140 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009141 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009142 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009143 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009144 Sua Conta Corrente nao esta autorizada para este servico. 
1009145 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009146 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009147 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009148 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009149 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009150 Sua Conta Corrente nao esta autorizada para este servico. 
1009151 Quantidade de Taloes invalida. Retorne a pagina anterior e tente novamente. 
1009152 Sua Conta Corrente nao esta autorizada para este servico. 
1009153 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009154 Sua Conta Corrente nao esta autorizada para este servico. 
1009155 Sua Conta Corrente nao esta autorizada para este servico. 
1009165 Nao existe nenhum pedido de talao pendente. 
1009166 O numero do cheque nao foi informado corretamente. Retorne a pagina anterior e tente novamente. 
1009167 Este pedido de talao ja esta desbloqueado. 
1009169 Este talao foi cancelado. 
1009170 Sua Conta Corrente nao esta autorizada para este servico. 
1009171 Este talao foi cancelado. 
1009172 Este talao foi cancelado. 
1009174 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009175 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009176 Sua Conta Corrente encontra-se com problemas cadastrais. Procure logo o seu Gerente e faca o recadastramento. 
1009192 Ja existe uma solicitacao de talao pendente. 
1009193 O servico solicitado esta fora do horario permitido. 
